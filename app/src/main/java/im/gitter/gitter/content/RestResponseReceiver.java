package im.gitter.gitter.content;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.LongSparseArray;

import java.util.ArrayList;
import java.util.List;

import static im.gitter.gitter.content.RestServiceHelper.REQUEST_ID_INTENT_KEY;
import static im.gitter.gitter.content.RestServiceHelper.RESULT_CODE_INTENT_KEY;

public class RestResponseReceiver extends BroadcastReceiver {

    private LongSparseArray<List<Listener>> listenerMap = new LongSparseArray<>();

    @Override
    public void onReceive(Context context, Intent intent) {
        Long requestId = intent.getLongExtra(REQUEST_ID_INTENT_KEY, -1);
        int statusCode = intent.getIntExtra(RESULT_CODE_INTENT_KEY, -1);

        List<Listener> listeners = listenerMap.get(requestId);

        if (listeners != null) {
            for (Listener listener : listeners) {
                listener.onResponse(statusCode);
            }
            listenerMap.remove(requestId);
        }
    }

    public IntentFilter getFilter() {
        return new IntentFilter(RestServiceHelper.INTENT_ACTION);
    }

    public void listen(long requestId, Listener listener) {
        List<Listener> listeners = listenerMap.get(requestId, new ArrayList<Listener>());
        listeners.add(listener);
        listenerMap.put(requestId, listeners);
    }

    public interface Listener {
        void onResponse(int statusCode);
    }

    public static abstract class SuccessFailureListener implements Listener {

        @Override
        public void onResponse(int statusCode) {
            if (statusCode < 400) {
                onSuccess();
            } else {
                onFailure(statusCode);
            }
        }

        public abstract void onSuccess();
        public abstract void onFailure(int statusCode);
    }

    public static abstract class FailureListener extends SuccessFailureListener {
        @Override
        public void onSuccess() {
            // ignore
        }
    }
}
